package compiler;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Counter {

    /**
     * Count the occurrences of {@code reservedWords}, {@code operators}, {@code variables} and chains of strings
     * (quoted ones) in the specified {@code input}.
     * @param input the text where we'll search
     * @param reservedWords the reserved words accepted on our compiler
     * @param operators the operators accepted on our compiler
     * @param variables the variables of the program
     * @return a key-count map, where key is the found item (reserved word, operator, variable or string) and count is
     * the number of times it occurs in the input.
     */
    public static HashMap<String, Long> count(final String input,
                                              final List<String> reservedWords,
                                              final List<String> operators,
                                              final List<String> variables) {

        final HashMap<String, Long> matches = new HashMap<>();

        // We remove regions from remaining as we find each string, primarily to prevent duplicates in sub-strings
        final StringBuffer buffer = new StringBuffer(input);

        countAndRemoveStrings(matches, buffer);
        countRegularWords(matches, buffer, reservedWords);
        countRegularWords(matches, buffer, variables);
        countOperators(matches, buffer, operators);

        return matches;
    }

    /**
     * Match strings in {@code input} and remove found items from buffer.
     * @param counts the map where we'll put the keys and increment the found items
     * @param input the code where we'll search and then remove found items
     */
    private static void countAndRemoveStrings(final HashMap<String, Long> counts, final StringBuffer input) {

        Matcher stringMatcher = Pattern.compile("([\"'])(?:(?=(\\\\?))\\2.)*?\\1").matcher(input);
        while (stringMatcher.find()) {
                increaseFound(counts, stringMatcher.group());
        }
        String temp = stringMatcher.replaceAll("");
        input.setLength(0);
        input.append(temp);
    }

    /**
     * Match normal words (letter-based character chain) and count them.
     * @param counts the map where we'll put the keys and increment the found items
     * @param input the code where we'll search the words
     * @param words the list of words we need to search
     */
    private static void countRegularWords(final HashMap<String, Long> counts,
                                          final StringBuffer input,
                                          final List<String> words) {

        for (String word : words) {
            Matcher wordMatcher = Pattern.compile("\\b" + word).matcher(input);
            while (wordMatcher.find()) {
                increaseFound(counts, word);
            }
        }
    }

    /**
     * Match specific characters chains (or single ones) and count them.
     * @param counts the map where we'll put the keys and increment the found items
     * @param input the code where we'll search the operators
     * @param operators the list of operators we need to search
     */
    private static void countOperators(final HashMap<String, Long> counts,
                                       final StringBuffer input,
                                       final List<String> operators) {

        for (String operator : operators) {
            Matcher operatorMatcher = Pattern.compile("[" + operator + "]").matcher(input);
            while (operatorMatcher.find()) {
                increaseFound(counts, operator);
            }
        }
    }

    /**
     * Check if the map contains the {@code key} and then increment it; or put it with 1 as count
     * @param counts the map where we'll manage the keys
     * @param key the key we want to increment
     */
    private static void increaseFound(HashMap<String, Long> counts, String key) {
        if (counts.containsKey(key)) {
            long found = counts.get(key);
            found++;
            counts.put(key, found);
        } else {
            counts.put(key, 1L);
        }
    }
}
