import compiler.Counter;
import io.Input;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Read the file (you must specify it as the first argument of the program)
        final String input;
        try {
            input = Input.readFileAsString(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        // Here we declare all the things we want to search for
        final List<String> reservedWords = Arrays.asList("Se", "Entao", "FimSe", "Escreva");
        final List<String> operators     = Arrays.asList("(", ")", "<", ">", ":=");
        final List<String> variables     = Arrays.asList("A", "B", "temp");

        // Do the count
        final HashMap<String, Long> count = Counter.count(input, reservedWords, operators, variables);

        // Print a beautiful table
        String leftAlignFormat = "| %-15s | %-5d |%n";

        System.out.format("+-----------------+-------+%n");
        System.out.format("| Key             | Count |%n");
        System.out.format("+-----------------+-------+%n");
        for (String key : count.keySet()) {
            System.out.format(leftAlignFormat, key, count.get(key));
        }
        System.out.format("+-----------------+-------+%n");
    }
}
